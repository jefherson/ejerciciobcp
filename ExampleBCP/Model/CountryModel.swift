//
//  CountriesModel.swift
//  ExampleBCP
//
//  Created by everis on 1/8/21.
//

import Foundation

struct CountryModel {
    var countryName     : String?
    var currencyName    : String?
    var changeType      : [ChangeType]?
    var symbol          : String?
}

struct ChangeType {
    
    var nameCountry     : String?
    var imageCountry    : String?
    var purchase        : Double?
    var sale            : Double?
    var symbol          : String?
    var nameCurrency    : String?
}
