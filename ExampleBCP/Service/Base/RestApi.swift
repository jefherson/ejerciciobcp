//
//  RestApi.swift
//  ExampleBCP
//
//  Created by everis on 1/9/21.
//

import Foundation

public enum HTTPMethod: String {
    case options = "OPTIONS"
    case get     = "GET"
    case head    = "HEAD"
    case post    = "POST"
    case put     = "PUT"
    case patch   = "PATCH"
    case delete  = "DELETE"
    case trace   = "TRACE"
    case connect = "CONNECT"
}

protocol RestApi {
   
}

extension RestApi {
    
    var baseURL:String {
        if let url = Bundle.main.object(forInfoDictionaryKey: "Constants.urlkey") as? String {
            return url
        }
        return ""
    }
    
    var headers: [String : String] {
        let token = "Bearer jnasdpnajsdahdxzccxzc"
        
        return ["Content-Type":"Application/json",
                "Authorization": token]
        
    }
    
    func request(url: String,
                 verb: HTTPMethod,
                 parameters: [String:Any]? = nil,
                 headers:[String:String]? = nil,
                 success: @escaping (CountryModel) -> Void,
                 failure: @escaping (Error) ->Void) {
        
        executeRequest(url: url, verb: verb, parameters: parameters, headers: headers, success: success, failure: failure)
    
    }

    private func executeRequest(url:String,
                                verb:HTTPMethod,
                                parameters:[String:Any]? = nil,
                                headers:[String:String]? = nil,
                                success: @escaping (CountryModel) -> Void,
                                failure: @escaping (Error) -> Void) {
       
        let serviceHeaders = headers ?? self.headers
        
        success(createModel())
        
        /* pense en hacer un llamado de servicio en un server mock o algo pero ya no me alcanzo el timepo , sorry.
          por eso solo deje contruido la clas ResAPI
        */
        
    }
    
    private func createModel() -> CountryModel{

        var countryModel = CountryModel()
        
        var changeType = [ChangeType]()
        let unitedState = ChangeType(nameCountry: "United State", imageCountry: "UnitedState", purchase: 3.41, sale: 3.20, symbol: "USD", nameCurrency: "Dolar Americano")
        let japan = ChangeType(nameCountry: "Japan",imageCountry: "Japan", purchase: 1.20, sale: 1.21, symbol: "JPY", nameCurrency: "Yenes")
        let european = ChangeType(nameCountry: "European union", imageCountry: "European", purchase: 3.50, sale: 2.92, symbol: "EUR", nameCurrency: "Euros")
        let canada = ChangeType(nameCountry: "Canada", imageCountry: "Canada", purchase: 4.49, sale: 4.20, symbol: "CAD", nameCurrency: "Dolar Canadiense")
        let unitedKingdom = ChangeType(nameCountry: "United Kigndom", imageCountry: "UnitedKigndom", purchase: 5.40, sale: 5.26, symbol: "GBP", nameCurrency: "Dolar Ingles")
        let switzerland = ChangeType(nameCountry: "Switzerland", imageCountry: "Switzerland", purchase: 6.43, sale: 6.21, symbol: "CHF", nameCurrency: "Dolar Swizo")
        
        changeType.append(unitedState)
        changeType.append(european)
        changeType.append(japan)
        changeType.append(canada)
        changeType.append(unitedKingdom)
        changeType.append(switzerland)
        
        countryModel = CountryModel(countryName: "Peru", currencyName: "Soles", changeType: changeType, symbol: "S/")
        
        return countryModel
    }
}

