//
//  Constants.swift
//  ExampleBCP
//
//  Created by everis on 1/9/21.
//

import Foundation

struct Constants {
    
    struct Messages {
        static let error = "error al cargar la información"
    }

    struct NibNames {
        static let StepTwoCell = "StepTwoCell"
    }
    struct Helpers {
        static let NoImage = "NoImage"
    }
}
