//
//  String+Extension.swift
//  ExampleBCP
//
//  Created by everis on 1/8/21.
//

import Foundation

extension String {
    
    func text(before text: String) -> String? {
        guard let range = self.range(of: text) else { return nil }
        return String(self[self.startIndex..<range.lowerBound])
    }
}
