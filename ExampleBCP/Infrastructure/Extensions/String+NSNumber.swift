//
//  String+NSNumber.swift
//  ExampleBCP
//
//  Created by everis on 1/8/21.
//

import Foundation

// MARK: - String to Number
extension String {
    
    public init(currency:NSNumber, withSymbol symbol:String? = nil, grouped:Bool = true, locale:Locale = Locale.current) {
        let currencySymbol = symbol ?? ""
        self = NumberFormatter.shared.currency(fromNumber: currency, isGrouped: grouped, symbol: currencySymbol, locale: locale)
    }
    
    public init(number:NSNumber, grouped:Bool = true, locale:Locale = Locale.current) {
        self = NumberFormatter.shared.numericString(fromNumber: number, isGrouped: grouped, locale: locale)
    }
    
    public init(wholeNumber:NSNumber, grouped:Bool = true, locale:Locale = Locale.current) {
        self = NumberFormatter.shared.wholeNumericString(fromNumber: wholeNumber, isGrouped: grouped, locale: locale)
    }
    
}
