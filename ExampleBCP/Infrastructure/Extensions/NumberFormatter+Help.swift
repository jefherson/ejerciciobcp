//
//  NumberFormatter+Help.swift
//  ExampleBCP
//
//  Created by everis on 1/8/21.
//

import Foundation

extension NumberFormatter {
    
    // MARK: - Singleton
    public static let shared = NumberFormatter()
    
    // MARK: - Number to String
    public func currency(fromNumber number:NSNumber, isGrouped:Bool = true, symbol:String = "", locale:Locale = Locale.current) -> String {
        self.formatterBehavior = .behavior10_4
        self.numberStyle = .decimal
        self.roundingMode = .halfUp
        self.locale = locale
        self.positiveFormat = isGrouped ? "#,##0.00" : "#0.00"
        let currency = self.string(from: number)!
        
        let currencySymbol = symbol.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
        if currencySymbol != "" {
            return "\(currencySymbol) \(currency)"
        }
        return currency
    }
    
    public func numericString(fromNumber number:NSNumber, isGrouped:Bool = true, locale:Locale = Locale.current) -> String {
        self.formatterBehavior = .behavior10_4
        self.numberStyle = .decimal
        self.roundingMode = .floor
        self.positiveFormat = nil
        self.locale = locale
        self.usesGroupingSeparator = isGrouped
        return string(from: number)!
    }
    
    public func wholeNumericString(fromNumber number:NSNumber, isGrouped:Bool = true, locale:Locale = Locale.current) -> String {
        self.formatterBehavior = .behavior10_4
        self.numberStyle = .decimal
        self.roundingMode = .floor
        self.locale = locale
        self.positiveFormat = isGrouped ? "#,##0" : "#0"
        return string(from: number)!
    }
    
    // MARK: - String to number
    public func number(fromNumericString numericString:String) -> NSNumber? {
        return number(fromLocaleNumericString: numericString, locale: Locale(identifier: "en_US"))
    }
    
    public func number(fromLocaleNumericString numericString:String, locale:Locale = Locale.current) -> NSNumber? {
        self.formatterBehavior = .behavior10_4
        self.numberStyle = .decimal
        self.positiveFormat = nil
        self.usesGroupingSeparator = true
        self.locale = locale
        return self.number(from:numericString)
    }
    
}
