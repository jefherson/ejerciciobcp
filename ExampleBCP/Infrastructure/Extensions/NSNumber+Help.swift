//
//  NSNumber+Help.swift
//  ExampleBCP
//
//  Created by everis on 1/8/21.
//

import UIKit

extension Bool {
    
    public var numberValue:NSNumber {
        return NSNumber(value: self)
    }
    
}

extension Int {
    
    public var numberValue:NSNumber {
        return NSNumber(value: self)
    }
    public var stringValue : String{
        return String(self)
    }
    
}

extension Decimal {
    
    public var numberValue:NSNumber {
        return NSDecimalNumber(decimal: self)
    }
   
    
}

extension Float {
    
    public var numberValue:NSNumber {
        return NSNumber(value: self)
    }
    
    public var cgFloatValue:CGFloat {
        return CGFloat(self)
    }

}

extension Double {
    
    public var numberValue:NSNumber {
        return NSNumber(value: self)
    }
    
    public var cgFloatValue:CGFloat {
        return CGFloat(self)
    }
    
}

extension CGFloat {
    
    public var numberValue:NSNumber {
        return NSNumber(value: doubleValue)
    }
    
    public var doubleValue:Double {
        return Double(self)
    }
    
}
