//
//  Observable.swift
//  ExampleBCP
//
//  Created by everis on 1/8/21.
//

import Foundation
import UIKit

public struct ObservableCall<Input> {
    
    private var callBack: ((Input)-> Void)?
    
    public mutating func bind<Observer: AnyObject>(to observer: Observer, with callback: @escaping (Observer, Input)-> Void) {
        self.callBack = { [weak observer] input in
            guard let observer = observer else {
                return
            }
            callback(observer, input)
        }
    }
    
    public func notify(with input: Input) {
        callBack?(input)
    }
    
    public init() {}
}
