//
//  UIButtonCustom.swift
//  ExampleBCP
//
//  Created by everis on 1/8/21.
//

import UIKit

@IBDesignable class UIButtonCustom: UIButton {
    
   
    @IBInspectable var atCircle: Bool = false {
        didSet {
            if atCircle {
                layer.cornerRadius = bounds.size.width / 2
            }
        }
    }
    
    @IBInspectable var borderColor : UIColor = .black {
        didSet {
            layer.borderColor  = borderColor.cgColor
        }
    }
    
    @IBInspectable var borderWidth : CGFloat = 1 {
        didSet {
            
            layer.borderWidth = borderWidth
            
        }
    }
    
    @IBInspectable var cornerRadius: CGFloat = 0 {
        didSet {
            layer.cornerRadius = cornerRadius
        }
    }
    
    @IBInspectable var shadowColor: UIColor = .black {
        didSet {
            layer.shadowColor = shadowColor.cgColor
        }
    }
    
    @IBInspectable var shadowRadius: CGFloat = 1 {
        didSet {
            layer.shadowRadius = shadowRadius
        }
    }
    
    @IBInspectable var shadowOpacity: Float = 1 {
        didSet {
            layer.shadowOpacity = shadowOpacity
        }
    }
    
    @IBInspectable var shadowOffset: CGSize {
        get {
            return self.layer.shadowOffset
        }
        set {
            self.layer.shadowOffset = newValue
        }
    }
    
    //MARK - Constractor
    override init(frame: CGRect) {
        super.init(frame:frame)
    }
    
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
}
