//
//  StepTwoCell.swift
//  ExampleBCP
//
//  Created by everis on 1/8/21.
//

import UIKit

class StepTwoCell: UITableViewCell {

    @IBOutlet weak var imageCountry: UIImageView!
    @IBOutlet weak var countryNameLabel: UILabel!
    @IBOutlet weak var typeChangeLabel: UILabel!
    
    static let identifier = "StepTwoCell"
    
    var currentCurrency : CountryModel = CountryModel()
    
    var data : ChangeType! {
        didSet  {
            imageCountry.image = UIImage(named: data.imageCountry ?? Constants.Helpers.NoImage)
            countryNameLabel.text = data.nameCountry
            typeChangeLabel.text = getTypeChangeLabel(data: data)
        }
    }
    
    private func getTypeChangeLabel(data : ChangeType) -> String {
        
        guard let currentSymbol = currentCurrency.symbol,
              let dataSymbol = data.symbol,
              let dataBuy = data.purchase?.numberValue else {
            return Constants.Messages.error
        }
        
        return "\(String(currency: 1,withSymbol: currentSymbol)) = \(String(currency: dataBuy,withSymbol: dataSymbol))"
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
