//
//  StepTwoViewController.swift
//  ExampleBCP
//
//  Created by everis on 1/8/21.
//

import UIKit

protocol StepTwoViewControllerDelegate: class {
    func didSelectItem(itemSelected : ChangeType)
}

class StepTwoViewController: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    
    var country = CountryModel()
    weak var delegate: StepTwoViewControllerDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
    }
    
    private func setupUI() {
        let nib = UINib(nibName: Constants.NibNames.StepTwoCell, bundle: nil)
        tableView.register(nib, forCellReuseIdentifier: StepTwoCell.identifier)
        tableView.delegate = self
        tableView.dataSource = self
    }
}

extension StepTwoViewController : UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return country.changeType?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        guard let cell = tableView.dequeueReusableCell(withIdentifier: StepTwoCell.identifier, for: indexPath) as? StepTwoCell else {
            return UITableViewCell();
        }
        cell.currentCurrency = country
        cell.data = country.changeType?[indexPath.row]
        return cell
    }
}

extension StepTwoViewController : UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.delegate?.didSelectItem(itemSelected: country.changeType![indexPath.row])
        self.navigationController?.popViewController(animated: true)
    }
}
