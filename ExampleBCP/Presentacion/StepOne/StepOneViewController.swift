//
//  StepOneViewController.swift
//  ExampleBCP
//
//  Created by everis on 1/8/21.
//

import UIKit

class StepOneViewController: UIViewController {
    
    var viewModel: StepOneViewModelProtocol = StepOneViewModel()
    var typeTranssaction : TypeTransaction  = .purchase
    
    @IBOutlet weak var currencySendLabel: UILabel!
    @IBOutlet weak var currencyReceiveLabel: UILabel!
    @IBOutlet weak var amountSendTextField: UITextField!
    @IBOutlet weak var amountReceiveTextField: UITextField!
    @IBOutlet weak var purchaseAndSaleValuesLabel: UILabel!
    @IBOutlet var viewWhitGesture: [UIView]!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupBuilding()
        viewModel.loadData()
        setupUI()
    }
    
    private func setupUI() {
        currencySendLabel.text = viewModel.countryModel.currencyName;
        currencyReceiveLabel.text = viewModel.changeTypeSelected.nameCurrency
        amountSendTextField.delegate = self
        amountSendTextField.keyboardType = .decimalPad
        viewModel.updateChangeValuesLabel()
        viewWhitGesture.forEach { (view) in view.addGestureRecognizer(setGestureRecognizer())}
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(hiddenKeyboard))
        self.view.addGestureRecognizer(tapGesture)
    }
    
    private func setupBuilding() {
        
        viewModel.updateReceiveAmount.bind(to: self) { (self, amount) in
            self.amountReceiveTextField.text = amount
        }
        
        viewModel.updateLabelWhitChangeValues.bind(to: self) { (self, message) in
            self.purchaseAndSaleValuesLabel.text = message
        }
        
        viewModel.updateNamesCurrencies.bind(to: self) { (self, tuple) in
            self.currencySendLabel.text = tuple.send
            self.currencyReceiveLabel.text = tuple.receive
        }
    }
    
    func setGestureRecognizer() -> UILongPressGestureRecognizer {
        let lpgr = UILongPressGestureRecognizer(target: self, action: #selector(longPressed))
        lpgr.minimumPressDuration = 0.5
        lpgr.delaysTouchesBegan = true
        return lpgr
    }
    
    @objc func longPressed(gestureReconizer: UILongPressGestureRecognizer) {
        if gestureReconizer.state != UIGestureRecognizer.State.ended {
            let viewController: StepTwoViewController = StepTwoViewController.instantiate()
            viewController.country = viewModel.countryModel
            viewController.delegate = self
            typeTranssaction = .purchase
            navigationController?.pushViewController(viewController, animated: true)
            gestureReconizer.state = .ended
        }
    }
    
    @objc func hiddenKeyboard() {
        self.view.endEditing(true)
    }
    
    @IBAction func changeCurrency(_ sender: Any) {
                
        if (typeTranssaction == .purchase){typeTranssaction = .sale} else {typeTranssaction = .purchase}
        viewModel.changeNameCurrency(sendCurrencyName: currencySendLabel.text!, recieveCurrencyName: currencyReceiveLabel.text!)
        viewModel.calculateAmount(amount: amountSendTextField.text ?? "0", typeTransaction: typeTranssaction)
    }
    
    
}
extension StepOneViewController : StepTwoViewControllerDelegate {
    func didSelectItem(itemSelected: ChangeType) {
        viewModel.changeTypeSelected = itemSelected
        viewModel.calculateAmount(amount: amountSendTextField.text ?? "0", typeTransaction: typeTranssaction)
        viewModel.updateAfterSelectNewCurrencyNames()
    }
}

extension StepOneViewController : UITextFieldDelegate {
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        let newString = (textField.text! as NSString).replacingCharacters(in: range, with: string)
        if newString.count > 10 { return false}
        viewModel.calculateAmount(amount: newString as String, typeTransaction: typeTranssaction)
        return true
    }
}
