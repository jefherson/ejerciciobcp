//
//  StepOneViewModel.swift
//  ExampleBCP
//
//  Created by everis on 1/8/21.
//

import Foundation

enum TypeTransaction {
    case purchase
    case sale
}

protocol StepOneViewModelProtocol {
    
    typealias namesTuple = (send: String, receive: String)
    
    // properties
    var changeTypeSelected  : ChangeType  { get set }
    var countryModel        : CountryModel {get set}
    
    // OutBound
    var updateReceiveAmount         : ObservableCall<String> { get set }
    var updateLabelWhitChangeValues : ObservableCall<String> { get set }
    var updateNamesCurrencies       : ObservableCall<namesTuple> { get set }
    
    // Inbound
    func loadData()
    func calculateAmount(amount: String,typeTransaction : TypeTransaction)
    func changeNameCurrency(sendCurrencyName: String, recieveCurrencyName : String)
    func updateChangeValuesLabel()
    func updateAfterSelectNewCurrencyNames()
}

class StepOneViewModel: StepOneViewModelProtocol {
 
    // OutBound
    var updateReceiveAmount          = ObservableCall<String>()
    var updateLabelWhitChangeValues  = ObservableCall<String>()
    var updateNamesCurrencies        = ObservableCall<namesTuple>()
    var updateAfterSelectNewCurrency = ObservableCall<namesTuple>()
    
    // properties
    var changeTypeSelected = ChangeType()
    var countryModel       = CountryModel()
    var totalReceive       = 0.0
    
    // Inbound
    
    func loadData() {
    
        CountriesManager.shared.getDataCountries { (countryModel) in
            self.countryModel = countryModel
            self.changeTypeSelected = countryModel.changeType?[0] ?? ChangeType()
        } failure: { (error) in
            print(error)
        }
    }
    
    func updateAfterSelectNewCurrencyNames() {
        updateChangeValuesLabel()
        self.updateNamesCurrencies.notify(with: (countryModel.currencyName!,changeTypeSelected.nameCurrency!))
    }
    
    func changeNameCurrency(sendCurrencyName: String, recieveCurrencyName: String) {
 
        self.updateNamesCurrencies.notify(with: (recieveCurrencyName,sendCurrencyName))
    }
    
    func updateChangeValuesLabel() {
        
        let message = "Compra: \(String(currency:  changeTypeSelected.purchase?.numberValue ?? 0.0)) | Venta: \(String(currency: changeTypeSelected.sale?.numberValue ?? 0.0))"
        updateLabelWhitChangeValues.notify(with: message)
    }
  
    func calculateAmount(amount: String, typeTransaction: TypeTransaction) {
       
        switch typeTransaction {
        case .purchase:
            totalReceive = (Double(amount) ?? 0.0) / (changeTypeSelected.purchase ?? 0.0)
            break
        case .sale:
            totalReceive = (Double(amount) ?? 0.0) * (changeTypeSelected.sale ?? 0.0)
            break
        }
        
        self.updateReceiveAmount.notify(with: String(currency: totalReceive.numberValue))
    }
    
}
