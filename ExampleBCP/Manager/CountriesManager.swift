//
//  CountriesManager.swift
//  ExampleBCP
//
//  Created by everis on 1/9/21.
//

import Foundation

class CountriesManager {
    
    // MARK: - Singleton
    static let shared = CountriesManager()
    
    // MARK: - SERVICE
    let service = CountriesServiceRepository()
        
    // MARK: - GetCountries
    func getDataCountries(success: @escaping (CountryModel) -> Void,
                      failure: @escaping (Error) -> Void) {
        
        service.getDataCountries ( success: { (serviceResponse) in
            
            
            success(serviceResponse)
            
        } , failure: failure)
    }
}
